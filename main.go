package main

import (
	"bitbucket.org/sothy5/go-code/internal"
	"github.com/gin-gonic/gin"
	stats "github.com/semihalev/gin-stats"
	"net/http"
)

func setupRouter() *gin.Engine {
	gin.SetMode(gin.ReleaseMode)
	r := gin.Default()
	r.Use(stats.RequestStats())

	// POST to calculate fizzbuzz
	r.POST("/fizzbuzz", func(c *gin.Context) {
		var inputs algo.Inputs
		c.BindJSON(&inputs)

		result, _ := algo.MainComputeFunction(&inputs)
		c.JSON(http.StatusOK, gin.H{
			"result": result,
		})
	})

	// stats APIs
	r.GET("/stats", func(c *gin.Context) {
		c.JSON(http.StatusOK, stats.Report())
	})

	return r
}

func main() {
	r := setupRouter()
	// Listen and Server in 0.0.0.0:8080
	r.Run(":8080")
}
