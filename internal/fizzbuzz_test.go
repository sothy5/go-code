package algo

import (
	"strings"
	"testing"
)

func algo_test(t *testing.T) {
	testData := Inputs{
		InputInt1: 2,
		InputInt2: 3,
		Limit:     10,
		String1:   "Test",
		String2:   "exam",
	}

	expected := string("1,Test,exam,4,5,Testexam,7,Test,exam,Test")
	actual, _ := MainComputeFunction(&testData)
	t.Log(actual)
	if strings.Compare(actual, expected) != 0 {
		t.Errorf("expected string %s for input %+v but got %s", expected, testData, actual)
	}

}
