package algo

import (
	"bytes"
	"strconv"
)

type Inputs struct {
	InputInt1 int    `json:"inputInt1"`
	InputInt2 int    `json:"inputInt2"`
	Limit     int    `json:"limit"`
	String1   string `json:"string1"`
	String2   string `json:"string2"`
}

// MainComputeFunction performs calculation based in two intergers, limit and two strings
func MainComputeFunction(i *Inputs) (string, error) {

	var b bytes.Buffer

	for n := 1; n < i.Limit; n++ {
		if n%(i.InputInt1*i.InputInt2) == 0 {
			b.WriteString(i.String1)
			b.WriteString(i.String2)
		} else if n%i.InputInt1 == 0 {
			b.WriteString(i.String1)
		} else if n%i.InputInt2 == 0 {
			b.WriteString(i.String2)
		} else {
			b.WriteString(strconv.Itoa(n))
		}

		b.WriteString(",")

	}
	return b.String(), nil
}
